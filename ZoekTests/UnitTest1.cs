﻿using System;
using System.IO;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SQLite;
using Taggy.Classes;

namespace ZoekTests
{
    [TestClass]
    public class UnitTest1
    {
        protected SQLiteConnection connection;
        [TestInitialize]
        public void TestSetUp()
        {
            this.connection = new SQLiteConnection("Data Source = gevulde_testdatabase.sqlite3");
            connection.Open();
        }

        [TestMethod]
        public void TestFileOpenen()
        {
            TaggyFile file = new TaggyFile(1, connection); //s

            Assert.AreEqual("testbestandjes\\wit.png", file.Path);
            Assert.IsFalse(file.Broken);
        }

        [TestMethod]
        public void TestEntryOpenen()
        {
            TaggyEntry entry = new TaggyEntry(1, connection); //s
            TaggyFile file = new TaggyFile(1, connection); //s

            Assert.AreEqual(entry.File.ID, file.ID);
        }

        [TestMethod]
        public void TestTagOpvragen()
        {
            TaggyTag tag = new TaggyTag("A", connection);

            Assert.AreEqual("A", tag.Name);
            Assert.IsFalse(tag.IsAlias);
            Assert.IsNull(tag.Parent);
        } 
        [TestMethod]
        public void TestTagVerbinding()
        {
            TaggyEntry entry = new TaggyEntry(1, connection); //s, alle tags
            Assert.AreEqual("testbestandjes\\wit.png", entry.File.Path);
            TaggyTag taga = new TaggyTag("A", connection);
            TaggyTag tagb = new TaggyTag("B", connection);
            TaggyTag tagc = new TaggyTag("C", connection);

            Assert.IsTrue(entry.HasTag(taga));
            Assert.IsTrue(entry.HasTag(tagb));
            Assert.IsTrue(entry.HasTag(tagc));

            entry = new TaggyEntry(8, connection);   //z, geen tags
            Assert.AreEqual("testbestandjes\\zwart.png", entry.File.Path);
            Assert.IsFalse(entry.HasTag(taga));
            Assert.IsFalse(entry.HasTag(tagb));
            Assert.IsFalse(entry.HasTag(tagc));

            entry = new TaggyEntry(4, connection);
            Assert.AreEqual("testbestandjes\\roze.png", entry.File.Path);
            Assert.IsTrue(entry.HasTag(taga));
            Assert.IsTrue(entry.HasTag(tagb));
            Assert.IsFalse(entry.HasTag(tagc));
        }

        [TestMethod]
        public void TestZoekopdrachtAlleTags()
        {
            TaggySearchEngine zoeker = new TaggySearchEngine(connection);
            var search = zoeker.ParseSearchQuery("A B C");
            SQLiteCommand cmd = zoeker.MaakQuery(search[0]);

            SQLiteDataReader result = cmd.ExecuteReader();
            Assert.IsTrue(result.Read());

            Assert.AreEqual((Int64)1, result["id"]);
            TaggyEntry entry = new TaggyEntry((Int64)result["id"], connection);
            TaggyFile file = new TaggyFile((Int64)result["file_id"], connection);
            Assert.AreEqual(file.ID, entry.File.ID);

            Assert.IsFalse(result.Read());
        }

        [TestMethod]
        public void TestThumbnailer()
        {
            if (!Directory.Exists("thumbnails"))
                Directory.CreateDirectory("thumbnails");
            SQLiteCommand cmd = new SQLiteCommand("SELECT id FROM files", connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var file = new TaggyFile((long)reader["id"], connection);
                Console.WriteLine(file.Thumbnail.Size);
                Assert.AreEqual(new Size(200, 200), file.Thumbnail.Size);
            }
        }
    }
}
