﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Taggy
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Maak database, datamappen aan als niet bestaat
            string dataPath = Properties.Settings.Default["DataPath"].ToString();
            if (dataPath == "" || !Directory.Exists(dataPath))
            {
                dataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TaggyData");
                Properties.Settings.Default["DataPath"] = dataPath;
                Directory.CreateDirectory(dataPath);
                string dbpath = Path.Combine(
                    dataPath,
                    Properties.Settings.Default["DatabaseName"].ToString()
                    );
                Properties.Settings.Default["DatabasePath"] = dbpath;
            }

            if (!Directory.Exists(Path.Combine(dataPath, "thumbnails")))
                Directory.CreateDirectory(Path.Combine(dataPath, "thumbnails"));
            if (!File.Exists(Properties.Settings.Default["DatabasePath"].ToString()))
            {
                using (SQLiteConnection conn = new SQLiteConnection($"Data Source = {Properties.Settings.Default["DatabasePath"]}"))
                {
                    conn.Open();
                    using (SQLiteCommand tableCreator = new SQLiteCommand(Properties.Resources.DB_schema, conn))
                    {
                        tableCreator.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new taggysearchform());
        }
    }
}
