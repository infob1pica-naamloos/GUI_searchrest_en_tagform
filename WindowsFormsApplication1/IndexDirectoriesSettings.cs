﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using Taggy.Classes;

namespace Taggy
{
    /// <summary>
    /// Form om de geïndexeerde mappen te beheren en het indexeren uit te voeren.
    /// </summary>
    public partial class IndexDirectoriesSettings : Form
    {
        static string DB_CONNECTION = $"Data Source = {Properties.Settings.Default["DatabasePath"]}";
        SQLiteConnection conn;

        public IndexDirectoriesSettings()
        {
            Icon = Properties.Resources.icoon;
            conn = new SQLiteConnection(DB_CONNECTION);
            conn.Open();
            InitializeComponent();
        }

        /// <summary>
        /// Loopt alle mappen die geïndexeerd moeten worden na.
        /// </summary>
        public void PerformIndexSearch()
        {
            using (SQLiteTransaction tr = conn.BeginTransaction())
            { 
                string sql = "SELECT id,path,taglist,last_indexed FROM autoindex_directories";
                using (SQLiteCommand indexDirectories = new SQLiteCommand(sql, conn))
                {
                    SQLiteDataReader reader = indexDirectories.ExecuteReader();
                    while (reader.Read())
                    {
                        // Alle tags die aan bestanden moeten worden gekoppeld worden één keer aangemaakt per map
                        string taglist = reader["taglist"].ToString();
                        string[] tags_string = taglist.Split();
                        List<TaggyTag> tags = new List<TaggyTag>();
                        foreach (string tagname in tags_string)
                        {
                            TaggyTag t = new TaggyTag(tagname, conn);
                            t.Save();
                            tags.Add(t);
                        }

                        // Loop bestanden na, als later dan vorige scan zijn gemaakt wordt bestand toegevoegd.
                        DateTime previous = (DateTime)reader["last_indexed"];
                        foreach (string f in Directory.EnumerateFiles((string)reader["path"]))
                        {
                            DateTime created = File.GetCreationTime(f);
                            if (created > previous)
                            {
                                TaggyFile tf = new TaggyFile(f, conn);
                                TaggyEntry e = new TaggyEntry(tf, conn);
                                e.Save();
                                foreach (TaggyTag t in tags)
                                    e.AddTag(t);
                            }
                        }
                        string updateCommand = "UPDATE autoindex_directories SET last_indexed = CURRENT_TIMESTAMP WHERE id = @id";
                        SQLiteCommand updateTime = new SQLiteCommand(updateCommand, conn);
                        updateTime.Parameters.AddWithValue("id", reader["id"]);
                        updateTime.ExecuteNonQuery();
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Bouwt het form op wanneer het wordt geopend en leest de database in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IndexDirectoriesSettings_Load(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.MultiSelect = false;
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("Path", "Path");
            dataGridView1.Columns.Add("Tags", "Tags");
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            SQLiteCommand read = new SQLiteCommand("SELECT id,path,taglist FROM autoindex_directories", conn);
            SQLiteDataReader reader = read.ExecuteReader();
            while (reader.Read())
            {
                dataGridView1.Rows.Add(reader["id"], reader["path"], reader["taglist"]);
            }
        }

        /// <summary>
        /// Vraag gebruiker om een map en de tags erbij
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() != DialogResult.OK)
                return;

            string path = fbd.SelectedPath;
            List<TaggyTag> tags = Taggy.Classes.TaggySearchEngine.VraagTags(conn, true);
            if (tags.Count == 0) // Gebruiker klikte 'cancel'
                return;
            string taglist = string.Join(" ", tags);

            string sql = "INSERT INTO autoindex_directories (path, taglist, last_indexed) VALUES (@path, @taglist, '1970-01-01 01:00:00')";
            using (SQLiteCommand inserter = new SQLiteCommand(sql, conn))
            {
                inserter.Parameters.AddWithValue("@path", path);
                inserter.Parameters.AddWithValue("@taglist", taglist);
                inserter.ExecuteNonQuery();
            }
            IndexDirectoriesSettings_Load(null, null);
        }

        /// <summary>
        /// Om problemen met indexeren te voorkomen blijft het form op de achtergrond actief na sluiting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, EventArgs e)
        {
            Hide();
        }

        /// <summary>
        /// Map verwijderen is pas mogelijk wanneer er een rij is geselecteerd.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            removeButton.Enabled = true;
        }

        /// <summary>
        /// Verwijdert een index-vermelding van een map, maar niet de bestanden eruit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dataGridView1.SelectedRows[0];
            long id = (long)row.Cells[0].Value;
                using (SQLiteTransaction tr = conn.BeginTransaction())
                {
                    SQLiteCommand deleteCommand = new SQLiteCommand(conn);
                    deleteCommand.CommandText = "DELETE FROM autoindex_directories WHERE id = @id";
                    deleteCommand.Parameters.AddWithValue("@id", id);
                    deleteCommand.ExecuteNonQuery();
                    tr.Commit();
                }
            IndexDirectoriesSettings_Load(null, null);
        }
    }
}
