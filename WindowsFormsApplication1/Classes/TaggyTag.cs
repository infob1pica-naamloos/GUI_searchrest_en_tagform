﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;

namespace Taggy.Classes
{
    public class TaggyTag : TaggyEntity
    {
        public string Name;
        public TaggyTag Parent;
        public bool IsAlias;

        /// <summary>
        /// Vraag tag met bekende id op uit database.
        /// </summary>
        /// <param name="id">ID van tag</param>
        /// <param name="conn">Verbinding met database</param>
        public TaggyTag(Int64 id, SQLiteConnection conn) : base(id, conn)
        {
            SQLiteCommand OpenCommand = new SQLiteCommand(
                "SELECT * FROM tags WHERE id = @id",
                connection
                );
            OpenCommand.Parameters.AddWithValue("@id", ID);

            SQLiteDataReader result = OpenCommand.ExecuteReader();
            bool found = result.Read();
            if (!found)
                throw new KeyNotFoundException();
            else
            {
                Name = (string)result["name"];

                if (result["parent"] is DBNull)
                    Parent = null;
                else
                    Parent = new TaggyTag((Int64)result["parent"], connection);

                IsAlias = (bool)result["is_alias"];
                if (IsAlias)
                {
                    Name = this.Parent.Name;
                    ID = this.Parent.ID;
                    unsaved = false;
                }
            }
        }

        /// <summary>
        /// Zoek tag met naam op uit database, maak nieuwe als niet bestaat. Niet hoofdlettergevoelig.
        /// </summary>
        /// <param name="name">Op te vragen naam</param>
        /// <param name="conn">Verbinding met database</param>
        public TaggyTag(string name, SQLiteConnection conn) : base(0, conn)
        {
            SQLiteCommand OpenCommand = new SQLiteCommand(
                "SELECT * FROM tags WHERE name LIKE @name",
                connection
                );
            OpenCommand.Parameters.AddWithValue("@name", name);

            SQLiteDataReader result = OpenCommand.ExecuteReader();
            bool found = result.Read();
            if (!found)
            {
                unsaved = true;
                Name = name;
                Parent = null;
                IsAlias = false;
            }
            else
            {
                ID = (Int64)result["id"];
                Name = (string)result["name"];

                if (result["parent"] is DBNull)
                    Parent = null;
                else
                    Parent = new TaggyTag((Int64)result["parent"], connection);
                IsAlias = (bool)result["is_alias"];
                if (IsAlias)
                {
                    Name = Parent.Name;
                    ID = Parent.ID;
                    unsaved = false;
                }
            }
        }

        /// <summary>
        /// Maak een lege tag.
        /// </summary>
        /// <param name="conn"></param>
        public TaggyTag(SQLiteConnection conn) : base(0, conn)
        {
            throw new NotImplementedException();
        }

        public override void Save()
        {
            if (!unsaved)
                return;

            SQLiteCommand SaveCommand;
            if (ID != 0)
            {
                SaveCommand = new SQLiteCommand(
                    "UPDATE tags SET name=@name,parent=@parent,is_alias=@isalias WHERE id=@id",
                    connection);
                SaveCommand.Parameters.AddWithValue("@id", ID);
            }
            else
            {
                SaveCommand = new SQLiteCommand(
                    "INSERT INTO tags (name,parent,is_alias) VALUES (@name,@parent,@isalias)",
                    connection
                    );
            }
            SaveCommand.Parameters.AddWithValue("@name", Name);
            SaveCommand.Parameters.AddWithValue("@isalias", IsAlias);
            if (Parent != null)
                SaveCommand.Parameters.AddWithValue("@parent", Parent.ID);
            else
                SaveCommand.Parameters.AddWithValue("@parent", null);
            
            int result = SaveCommand.ExecuteNonQuery();
            if (result != 1)
                throw new Exception(string.Format("There were multiple or no affected rows on save: {0}", result));
            if (ID == 0)
                ID = GetLastInsertedID();

            unsaved = false;
        }

        /// <summary>
        /// Verwijder tag uit database en alle verwijzingen naar deze tag
        /// </summary>
        public override void Delete()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();

            try
            {
                RemoveFromAllEntries();
                RemoveFromParent();

                SQLiteCommand DeleteCommand = new SQLiteCommand(
                    "DELETE FROM tags WHERE id = @id",
                    connection
                    );
                DeleteCommand.Parameters.AddWithValue("@id", ID);

                int result = DeleteCommand.ExecuteNonQuery();
                ID = 0;
                if (result != 1)
                    throw new Exception(string.Format("There were multiple or no affected rows on delete: {0}", result));
                transaction.Commit();
            }
            catch(SQLiteException e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Verwijder alle verbindingen tussen entries en deze tag
        /// </summary>
        public void RemoveFromAllEntries()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();

            try
            {
                if (ID == 0)
                    return;

                SQLiteCommand DeleteCommand = new SQLiteCommand(
                    "DELETE FROM tagmap WHERE tag_id = @id",
                    connection
                    );
                DeleteCommand.Parameters.AddWithValue("@id", ID);

                DeleteCommand.ExecuteNonQuery();
                transaction.Commit();
            }
            catch(SQLiteException e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Zorg dat geen tags deze tag meer als parent hebben.
        /// </summary>
        public void RemoveFromParent()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();

            try
            {
                SQLiteCommand UpdateCommand = new SQLiteCommand(
                    "UPDATE tags SET parent = NULL WHERE parent = @id",
                    connection);
                UpdateCommand.Parameters.AddWithValue("@id", ID);
                UpdateCommand.ExecuteNonQuery();
                transaction.Commit();
            }
            catch(SQLiteException e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Maak lijst van alle entries met deze tag
        /// </summary>
        /// <returns>List van Entries, elke entry 1 keer.</returns>
        public List<TaggyEntry> GetEntries()
        {
            List<TaggyEntry> result = new List<TaggyEntry>();

            SQLiteCommand ReadCommand = new SQLiteCommand(
                "SELECT DISTINCT entry_id FROM tagmap WHERE tag_id = @id",
                connection);
            ReadCommand.Parameters.AddWithValue("@id", this.ID);

            SQLiteDataReader dbresult = ReadCommand.ExecuteReader();
            while (dbresult.Read())
            {
                long entryid = (long)dbresult["entry_id"];
                result.Add(new TaggyEntry(entryid, connection));
            }

            return result;
        }

        /// <summary>
        /// Stel de parent van deze tag in, voeg parenttag toe aan alle entries met deze tag
        /// </summary>
        /// <param name="parent">Oudertag</param>
        public void SetParent(TaggyTag parent)
        {
            SQLiteCommand UpdateCommand = new SQLiteCommand(
                "UPDATE tags SET parent=@newparent WHERE id = @id",
                connection);
            UpdateCommand.Parameters.AddWithValue("@id", ID);
            if (parent != null)
                UpdateCommand.Parameters.AddWithValue("@newparent", parent.ID);
            else
                UpdateCommand.Parameters.AddWithValue("@newparent", null);
        }

        /// <summary>
        /// Stel in dat deze tag een alias is van een andere tag, zet alle verwijzingen naar deze tag om naar eigenlijke tag
        /// </summary>
        /// <param name="actualtag">Eigenlijke tag</param>
        public void MakeAlias(TaggyTag actualtag)
        {
            SQLiteCommand UpdateCommand = new SQLiteCommand(
                "UPDATE tagmap SET tag_id=@nieuwe WHERE tag_id=@oude",
                connection);
            SQLiteCommand UpdateCommand2 = new SQLiteCommand(
                "UPDATE tags SET parent = @nieuwe WHERE id=@oude",
                connection);

            UpdateCommand.Parameters.AddWithValue("@nieuwe", actualtag.ID);
            UpdateCommand.Parameters.AddWithValue("@oude", ID);
            UpdateCommand.ExecuteNonQuery();
            UpdateCommand2.ExecuteNonQuery();

            Parent = actualtag;
            IsAlias = true;
        }

        /// <summary>
        /// zorgt dat de naam van de tag wordt weergegeven zodat het gebruikt kan worden als string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
