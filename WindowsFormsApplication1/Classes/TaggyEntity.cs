﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;

namespace Taggy
{
    /// <summary>
    /// TaggyEntity is de abstracte basisklasse van alle objecten die in de database worden opgeslagen.
    /// </summary>
    public abstract class TaggyEntity
    {
        public Int64 ID;
        public SQLiteConnection connection;
        public bool unsaved;

        /// <summary>
        /// Vraag object op uit de database op basis van ID.
        /// </summary>
        /// <param name="id">ID van het op te vragen object</param>
        /// <param name="conn">Verbinding met de database, moet in this.connection komen.</param>
        protected TaggyEntity(long id, SQLiteConnection conn)
        {
            ID = id;
            connection = conn;
        }

        /// <summary>
        /// Sla, indien er wijzigingen zijn, het object op in de database.
        /// </summary>
        public abstract void Save();

        /// <summary>
        /// Verwijder het object uit de database.
        /// </summary>
        public abstract void Delete();

        /// <summary>
        /// Vraag de RowID van de laatste insert op
        /// </summary>
        /// <returns>long rowID</returns>
        protected long GetLastInsertedID()
        {
            long result;

            SQLiteCommand GetCommand = new SQLiteCommand(
                "SELECT last_insert_rowid()", connection);
            result = (long)GetCommand.ExecuteScalar();

            return result;
        }
    }

    /// <summary>
    /// Hulpklasse om twee taggyentities te vergelijken, controleert of de ID's hetzelfde zijn
    /// </summary>
    public class TaggyEntityComparer : EqualityComparer<TaggyEntity>
    {
        public override bool Equals(TaggyEntity x, TaggyEntity y)
        {
            return x.ID == y.ID;
        }

        public override int GetHashCode(TaggyEntity obj)
        {
            return (int)obj.ID;
        }
    }
}
