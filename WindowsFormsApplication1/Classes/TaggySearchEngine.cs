﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace Taggy.Classes
{
    public enum SortMethod
    {
        Database = 0,
        Added_Ascending = 1,            // Oudste entries eerst
        Added_Descending = 2,           // Nieuwste entries eerst
        LastChanged_Ascending = 3,      // Eerst gewijzigde entries eerst
        LastChanged_Descending = 4,     // Laatst gewijzigde entries eerst
    }

    /// <summary>
    /// Zet zoekopdrachten zoals ingevoerd door de gebruiker om in queries
    /// </summary>
    public class TaggySearchEngine
    {
        protected SQLiteConnection connection;

        public TaggySearchEngine(SQLiteConnection conn)
        {
            connection = conn;
        }

        /// <summary>
        /// Splitst een zoekopdracht in een List van (List,List) tuples met welke tags wel en niet moeten worden meegenomen.
        /// </summary>
        /// <param name="query">Zoekopdracht</param>
        /// <returns>Een List van Tuples (include,exclude), eerste item bevat positieve zoekopdracht, tweede de negatieve.</returns>
        public List<Tuple<List<TaggyTag>,List<TaggyTag>>> ParseSearchQuery(string query)
        {
            List<Tuple<List<TaggyTag>, List<TaggyTag>>> result = new List<Tuple<List<TaggyTag>, List<TaggyTag>>>();
            string[] subqueries = query.Split('|');

            foreach (string subquery in subqueries)
                result.Add(ParseSubQuery(subquery));

            return result;
        }

        /// <summary>
        /// Zet een subquery om in een Tuple met positieve en negatieve tags.
        /// </summary>
        /// <param name="query">Subquery, mag geen | bevatten.</param>
        /// <returns>Tuple(include,exclude)</returns>
        public Tuple<List<TaggyTag>, List<TaggyTag>> ParseSubQuery(string query)
        {
            List<TaggyTag> include = new List<TaggyTag>(), exclude = new List<TaggyTag>();
            string[] tokens = query.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);

            foreach (string token in tokens)
            {
                if (token[0] == '-')
                    exclude.Add(new TaggyTag(token.TrimStart('-'), connection));
                else
                    include.Add(new TaggyTag(token, connection));
            }

            return new Tuple<List<TaggyTag>, List<TaggyTag>>(include, exclude);
        }

        /// <summary>
        /// Geeft een popupscherm dat de gebruiker om een aantal tags gescheiden door spaties vraagt en maakt hier een list van.
        /// </summary>
        /// <param name="conn">Verbinding met de database</param>
        /// <param name="saveNewTags">Of niet-bestaande tags gelijk moeten worden aangemaakt. (anders krijg je een taggytag met id 0)</param>
        /// <param name="title">Wat er in de titel van het venster moet komen te staan</param>
        /// <param name="message">Bericht in het venster</param>
        /// <param name="default_input">Standaardlijst van tags in het venster</param>
        /// <returns></returns>
        public static List<TaggyTag> VraagTags(SQLiteConnection conn, bool saveNewTags = false, string title = "Enter tags", string message = "Enter tags, separated by spaces", string default_input = "")
        {
            List<TaggyTag> result = new List<TaggyTag>();
            string input = Microsoft.VisualBasic.Interaction.InputBox(
                message, title, default_input);
            TaggySearchEngine tse = new TaggySearchEngine(conn);
            result = tse.ParseSubQuery(input).Item1;

            if (saveNewTags)
                foreach (TaggyTag t in result)
                    t.Save();

            return result;
        }
        /// <summary>
        /// Maakt een SQLiteCommand van een Tuple van tags die wel en niet in het resultaat vereist zijn.
        /// </summary>
        /// <param name="spec">Een Tuple met de tags waarop gezocht is</param>
        /// <param name="sorter"></param>
        /// <returns></returns>
        public SQLiteCommand MaakQuery(Tuple<List<TaggyTag>,List<TaggyTag>> spec, SortMethod sorter = SortMethod.Database)
        {
            string query = "SELECT DISTINCT e.id,e.file_id,f.path FROM entries e JOIN files f ON f.id = e.file_id\n";

            List<TaggyTag> incl = spec.Item1;
            List<TaggyTag> excl = spec.Item2;
            string inclQuery = "", exclQuery = "";

            if (incl != null && incl.Count > 0)
            {
                inclQuery = "e.id IN (SELECT tm.entry_id FROM tagmap tm WHERE tm.tag_id IN (";
                for (int i = 0; i < incl.Count; i++)
                {
                    string parameterstring = "@incl" + i.ToString();
                    inclQuery += parameterstring;
                    if (i != incl.Count - 1)
                        inclQuery += ",";
                }
                inclQuery += ") GROUP BY tm.entry_id HAVING COUNT(tm.entry_id) = @inclnum)";
            }

            if (excl != null && excl.Count > 0)
            {
                exclQuery = "e.id NOT IN (SELECT DISTINCT tm.entry_id FROM tagmap tm WHERE tm.tag_id IN (";
                for (int i = 0; i < excl.Count; i++)
                {
                    string parameterstring = "@excl" + i.ToString();
                    exclQuery += parameterstring;
                    if (i != excl.Count - 1)
                        exclQuery += ",";
                }
                exclQuery += "))";
            }

            if (inclQuery != "" || exclQuery != "")
            {
                query += " WHERE ";
                query += inclQuery;
                if (inclQuery != "" && exclQuery != "")
                    query += " AND ";
                query += exclQuery;
            }

            Console.WriteLine(query);

            SQLiteCommand cmd = new SQLiteCommand(query, connection);

            for (int i = 0; i < incl.Count; i++)
            {
                string parameterstring = "@incl" + i.ToString();
                cmd.Parameters.AddWithValue(parameterstring, incl[i].ID);
            }

            if (incl.Count != 0)
                cmd.Parameters.AddWithValue("@inclnum", incl.Count);

            for (int i = 0; i < excl.Count; i++)
            {
                string parameterstring = "@excl" + i.ToString();
                cmd.Parameters.AddWithValue(parameterstring, excl[i].ID);
            }

            return cmd;
        }
    }
}
