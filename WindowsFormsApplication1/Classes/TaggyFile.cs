﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using ImageProcessor;
using ImageProcessor.Imaging;

namespace Taggy.Classes
{
    public class TaggyFile: TaggyEntity
    {
        /// <summary>
        /// Locatie van het bestand op schijf
        /// </summary>
        public string Path;
        /// <summary>
        /// Is het bestand op schijf onvindbaar?
        /// </summary>
        public bool Broken;

        public TaggyFile(Int64 id, SQLiteConnection conn) : base(id, conn)
        {
            SQLiteCommand OpenCommand = new SQLiteCommand(
                "SELECT * FROM files WHERE id = @id",
                connection
                );

            OpenCommand.Parameters.AddWithValue("@id", id);
            SQLiteDataReader result = OpenCommand.ExecuteReader();

            bool found = result.Read();
            if (!found)
                throw new KeyNotFoundException();
            else
            {
                Path = (string)result["path"];
                Broken = (bool)result["broken"];
                unsaved = false;
            }
        }
        /// <summary>
        /// maakt een nieuwe taggyentry aan die verwijst naar een bestand.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="conn"></param>
        public TaggyFile(string path, SQLiteConnection conn) : base(0, conn)
        {
            Path = path;
            Broken = false;
            unsaved = true;
            Save();
        }

        /// <summary>
        /// maakt een nieuew taggy entry aan die nog niet naar een bestand verwijst.
        /// </summary>
        /// <param name="conn"></param>
        public TaggyFile(SQLiteConnection conn) : base(0, conn)
        {
            Path = "";
            Broken = false;
            unsaved = true;
        }

        /// <summary>
        /// Sla het bestand op in de database.
        /// </summary>
        public override void Save()
        {
            if (!unsaved)
                return;

            SQLiteCommand SaveCommand;

            if (ID != 0)
            {
                // Object bestaat al in database, bijwerken:
                SaveCommand = new SQLiteCommand(
                    "UPDATE files SET path=@path,broken=@broken WHERE id=@id",
                    connection
                    );
                SaveCommand.Parameters.AddWithValue("@path", Path);
                SaveCommand.Parameters.AddWithValue("@broken", Broken);
                SaveCommand.Parameters.AddWithValue("@id", ID);
            }
            else
            {
                // Object moet worden aangemaakt in database:
                SaveCommand = new SQLiteCommand(
                    "INSERT INTO files (path, broken) VALUES (@path, @broken)",
                    connection
                    );
                SaveCommand.Parameters.AddWithValue("@path", Path);
                SaveCommand.Parameters.AddWithValue("@broken", Broken);
            }

            int result = SaveCommand.ExecuteNonQuery();

            if (result != 1)
                throw new Exception(string.Format("There were multiple or no affected rows on save: {0}", result)); //TODO: Specifiekere exception vinden

            if (ID == 0)
                ID = GetLastInsertedID();
            unsaved = false;
        }

        public void MarkBroken()
        {
            this.Broken = true;
            this.unsaved = true;
            this.Save();
        }

        /// <summary>
        /// Verwijder het bestand uit de database.
        /// </summary>
        public override void Delete()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();

            try
            {
                foreach (TaggyEntry TE in GetAllEntries())
                    TE.Delete();

            if (ID == 0)
                return;

            SQLiteCommand DeleteCommand = new SQLiteCommand(
                "DELETE FROM files WHERE id = @id",
                connection);
            DeleteCommand.Parameters.AddWithValue("@id", ID);

            int result = DeleteCommand.ExecuteNonQuery();
            if (result != 1)
                throw new Exception(string.Format("There were multiple or no affected rows on delete: {0}", result));
            ID = 0;
                transaction.Commit();
            }
            catch(SQLiteException e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Return alle entries die naar dit bestand verwijzen
        /// </summary>
        /// <returns></returns>
        public List<TaggyEntry> GetAllEntries()
        {
            List<TaggyEntry> result = new List<TaggyEntry>();

            SQLiteCommand ReadCommand = new SQLiteCommand(
                "SELECT DISTINCT id FROM entries WHERE file_id = @id",
                connection);
            ReadCommand.Parameters.AddWithValue("@id", ID);

            SQLiteDataReader dbresult = ReadCommand.ExecuteReader();

            while (dbresult.Read())
            {
                int id = (int)dbresult["id"];
                result.Add(new TaggyEntry(id, connection));
            }

            return result;
        }

        /// <summary>
        /// haalt path van de preview op die bij de file hoort.
        /// </summary>
        public string ThumbnailPath
        {
            get
            {
                return System.IO.Path.Combine(
                    Properties.Settings.Default["DataPath"].ToString(), 
                    "thumbnails", ID.ToString() + ".jpg"
                    );
            }
        }
        /// <summary>
        /// resized de afbeeldingen naar een thumbnail van 200 bij 200
        /// </summary>
        public Image Thumbnail
        {
            get
            {
                if (!File.Exists(ThumbnailPath) && !Broken)
                {
                    ImageProcessor.Imaging.Formats.ISupportedImageFormat fmt = new ImageProcessor.Imaging.Formats.JpegFormat { Quality = 70 };
                    ResizeLayer resizer = new ResizeLayer(new Size(200, 200), ResizeMode.Crop);
                    ImageFactory fact = new ImageFactory();
                    try
                    {
                        fact.Load(Path)
                            .Resize(resizer)
                            .Format(fmt)
                            .Save(ThumbnailPath);
                        fact.Dispose();
                    }
                    catch
                    {
                        fact.Dispose();
                        this.MarkBroken();
                        return Properties.Resources.rederror;
                    }
                }
                if (Broken)
                    return Properties.Resources.rederror;
                return Bitmap.FromFile(ThumbnailPath);
            }
        }
    }
}