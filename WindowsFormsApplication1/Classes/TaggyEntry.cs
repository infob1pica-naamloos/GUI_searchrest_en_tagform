﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;

namespace Taggy.Classes
{
    public class TaggyEntry : TaggyEntity
    {
        public TaggyFile File;
        public DateTime LastChanged, Added;

        /// <summary>
        /// Lees een entry met een bepaalde ID uit de database.
        /// </summary>
        /// <param name="id">ID om te openen</param>
        /// <param name="conn">Verbinding met database</param>
        public TaggyEntry(Int64 id, SQLiteConnection conn) : base (id, conn)
        {
            SQLiteCommand OpenCommand = new SQLiteCommand(
                "SELECT * FROM entries WHERE id = @id",
                connection
                );

            OpenCommand.Parameters.AddWithValue("@id", id);
            SQLiteDataReader result = OpenCommand.ExecuteReader();

            bool found = result.Read();
            if (!found)
                throw new KeyNotFoundException();
            else
            {
                Int64 fileID = (Int64)result["file_id"];
                File = new TaggyFile(fileID, connection);
                if (!(result["lastchanged"] is DBNull))
                    LastChanged = (DateTime)result["lastchanged"];
                if (!(result["added"] is DBNull))
                   Added = (DateTime)result["added"];
                unsaved = false;
            }
        }
        /// <summary>
        /// Maak een nieuwe entry die verwijst naar een bestand.
        /// </summary>
        /// <param name="file">Bestand om naar te verwijzen</param>
        /// <param name="conn">Verbinding met database</param>
        public TaggyEntry(TaggyFile file, SQLiteConnection conn) : base(0, conn)
        {
            File = file;
            Added = DateTime.Now;
            LastChanged = DateTime.Now;
            unsaved = true;
        }

        /// <summary>
        /// Sla de entry op in de database indien er wijzigingen zijn.
        /// </summary>
        public override void Save()
        {
            if (!unsaved)
                return;

            SQLiteCommand SaveCommand;

            if (ID != 0)
            {
                SaveCommand = new SQLiteCommand(
                    "UPDATE entries SET file_id=@fileid,lastchanged=CURRENT_TIMESTAMP WHERE id=@id",
                    connection
                    );
                SaveCommand.Parameters.AddWithValue("@id", this.ID);
                SaveCommand.Parameters.AddWithValue("@fileid", this.File.ID);
            }
            else
            {
                SaveCommand = new SQLiteCommand(
                    "INSERT INTO entries (file_id,added,lastchanged) VALUES (@fileid,@added,CURRENT_TIMESTAMP)",
                    connection);
                SaveCommand.Parameters.AddWithValue("@fileid", this.File.ID);
                SaveCommand.Parameters.AddWithValue("@added", this.Added);
            }

            int result = SaveCommand.ExecuteNonQuery();

            if (result != 1)
                throw new Exception(string.Format("There were multiple or no affected rows on save: {0}", result));

            if (ID == 0)
                ID = GetLastInsertedID();

            unsaved = false;
        }

        /// <summary>
        /// Werk de laast-gewijzigd-datum bij naar de huidige tijd.
        /// </summary>
        public void UpdateLastChanged()
        {
            if (this.ID == 0)
                return;
            using (SQLiteCommand cmd = new SQLiteCommand("UPDATE entries SET lastchanged=CURRENT_TIMESTAMP WHERE id=@id", connection))
            {
                cmd.Parameters.AddWithValue("@id", this.ID);
                cmd.ExecuteNonQuery();
            }
            this.LastChanged = DateTime.Now;
        }

        /// <summary>
        /// Verwijder de entry uit de database.
        /// </summary>
        public override void Delete()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();

            try
            {
                RemoveAllTags();
                if (ID == 0)
                    return;

                using (SQLiteCommand DeleteCommand = new SQLiteCommand("DELETE FROM entries WHERE id = @id", connection))
                {
                    DeleteCommand.Parameters.AddWithValue("@id", this.ID);

                int result = DeleteCommand.ExecuteNonQuery();
                if (result != 1)
                    throw new Exception(string.Format("There were multiple or no affected rows on delete: {0}", result));
                ID = 0;
                transaction.Commit();
            }
            }
            catch(SQLiteException e)
            { 
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Maak een lijst van alle tags die naar deze entry verwijzen.
        /// </summary>
        /// <returns>Een List van TaggyTags, elke tag komt 1 keer voor.</returns>
        public List<TaggyTag> GetTags()
        {
            List<TaggyTag> result = new List<TaggyTag>();

            using (SQLiteCommand ReadCommand = new SQLiteCommand("SELECT DISTINCT tag_id FROM tagmap WHERE entry_id = @id", connection))
            {
                ReadCommand.Parameters.AddWithValue("@id", this.ID);

            SQLiteDataReader dbresult = ReadCommand.ExecuteReader();

            while (dbresult.Read())
            {
                Int64 tagid = (Int64)dbresult["tag_id"];
                result.Add(new TaggyTag(tagid, connection));
            }
            }
            return result;
        }

        /// <summary>
        /// Bepaal of een bepaalde tag aan deze entry is verbonden.
        /// </summary>
        /// <param name="tag">De tag om te onderzoeken.</param>
        /// <returns></returns>
        public bool HasTag(TaggyTag tag)
        {
            using (SQLiteCommand ReadCommand = new SQLiteCommand("SELECT COUNT(*) FROM tagmap WHERE entry_id = @entryid AND tag_id = @tagid", connection))
            {
                ReadCommand.Parameters.AddWithValue("@entryid", this.ID);
            ReadCommand.Parameters.AddWithValue("@tagid", tag.ID);

            Int64 result = (Int64)ReadCommand.ExecuteScalar();

            return result > 0;
        }
        }

        /// <summary>
        /// Verbind een tag aan deze entry, en verbind eventuele parent- of eigenlijke tag
        /// </summary>
        /// <param name="tag">De tag om te verbinden.</param>
        public void AddTag(TaggyTag tag)
        {
            if (HasTag(tag))
                return;

            using (SQLiteCommand InsertCommand = new SQLiteCommand("INSERT INTO tagmap (entry_id,tag_id) VALUES (@entryid,@tagid)", connection))
            {
            InsertCommand.Parameters.AddWithValue("@entryid", this.ID);
            InsertCommand.Parameters.AddWithValue("@tagid", tag.ID);

            int result = InsertCommand.ExecuteNonQuery();

            if (tag.Parent != null)
                AddTag(tag.Parent);

            if (result != 1)
                throw new Exception(string.Format("Tag connection has failed. Affected rows: {0}", result));
            UpdateLastChanged();
        }

            this.UpdateLastChanged();
        }

        /// <summary>
        /// Verwijder de verbinding tussen deze entry en een tag.
        /// </summary>
        /// <param name="tag">De tag om te verwijderen.</param>
        public void RemoveTag(TaggyTag tag)
        {
            using (SQLiteCommand DeleteCommand = new SQLiteCommand( "DELETE FROM tagmap WHERE entry_id = @entryid AND tag_id = @tagid", connection))
            {
                DeleteCommand.Parameters.AddWithValue("@entryid", this.ID);
            DeleteCommand.Parameters.AddWithValue("@tagid", tag.ID);
            DeleteCommand.ExecuteNonQuery();
            UpdateLastChanged();
        }
            this.UpdateLastChanged();
        }

        /// <summary>
        /// Verwijder alle verbindingen tussen tags en deze entry.
        /// </summary>
        public void RemoveAllTags()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();

            try
            {
                if (ID == 0)
                    return;
                SQLiteCommand DeleteCommand = new SQLiteCommand(
                    "DELETE FROM tagmap WHERE entry_id = @id",
                    connection
                    );
                DeleteCommand.Parameters.AddWithValue("@id", ID);
                DeleteCommand.ExecuteNonQuery();
                transaction.Commit();
                this.UpdateLastChanged();
            }
            catch (SQLiteException e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}
