﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Collections.Generic;
using Taggy.Classes;
using Microsoft.VisualBasic;
using System.Data;

namespace Taggy
{
    public partial class resultformwcode : Form
    {
        DataTable table = new DataTable();
        ComboBox searchbox;
        SQLiteConnection conn = new SQLiteConnection($"Data Source = {Taggy.Properties.Settings.Default["DatabasePath"]}");
        ToolStripMenuItem editmenu;
        string TaggyUserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Taggy");
        BindingSource bindingsource1;
        DataGridView dataview;
        ContextMenu cm;
        IndexDirectoriesSettings idf;

        public resultformwcode()
        {
            conn.Open();
            idf = new IndexDirectoriesSettings();
            idf.PerformIndexSearch();
            this.Text = "Taggy";
            this.ClientSize = new Size(800, 600);
            this.MinimumSize = new Size(600, 450);
           
            PictureBox searchicon;
            MenuStrip menu;

            bindingsource1 = new BindingSource();
            searchicon = new PictureBox();
            searchbox = new ComboBox();
            
            searchbox.KeyDown += Searchbox_KeyDown;
            dataview = new DataGridView(); 
           // scrollbar1 = new VScrollBar();
            menu = new MenuStrip();
            cm = new ContextMenu();

            menu.Size = new Size(672, 25);
            menu.Dock = DockStyle.Top;
           
            ToolStripMenuItem filemenu = new ToolStripMenuItem("File");
            filemenu.Text = "File";
            editmenu = new ToolStripMenuItem("Edit");
            editmenu.Text = "Edit";
            editmenu.Enabled = false;
            ToolStripMenuItem dbmenu = new ToolStripMenuItem("Database Maintenance");
            dbmenu.Text = "Database Maintenance";
            ToolStripMenuItem helpmenu = new ToolStripMenuItem("Help");
            helpmenu.Text = "Help";
            helpmenu.Click += helpoverview;

            ToolStripMenuItem addfile = new ToolStripMenuItem("Add Files");
            addfile.Text = "Add Files";
            ToolStripMenuItem singlefile = new ToolStripMenuItem("Add Single File");
            singlefile.Text = "Add Single File";
            ToolStripMenuItem addmulti = new ToolStripMenuItem("Add Multiple Files");
            addmulti.Text = "Add Multiple Files";
            ToolStripMenuItem addfolder = new ToolStripMenuItem("Add Folder");
            addfolder.Text = "Add Folder";

            ToolStripMenuItem export = new ToolStripMenuItem("Export");
            export.Text = "Export";
            

            ToolStripMenuItem delete = new ToolStripMenuItem("Delete Entry");
            delete.Text = "Delete Entry";
            
            ToolStripMenuItem selectall = new ToolStripMenuItem("Select All Entries");
            selectall.Text = "Select All Entries";

            ToolStripMenuItem view = new ToolStripMenuItem("View");
            view.Text = "View";
            ToolStripMenuItem addSingleItem_toolstrip = new ToolStripMenuItem("Add Single Item...");
            addSingleItem_toolstrip.Click += AddSingleItem_Click;
            ToolStripMenuItem addMultipleItems_toolstrip = new ToolStripMenuItem("Add Multiple Items...");
            addMultipleItems_toolstrip.Click += addMultipleItems_Click;
            ToolStripMenuItem addEntireFolder = new ToolStripMenuItem("Add files from folder...");
            addEntireFolder.Click += addEntireFolder_Click;
            ToolStripMenuItem entryjump = new ToolStripMenuItem("Jump to entry ...");
            entryjump.Text = "Jump to entry ...";
            ToolStripMenuItem indexsettings = new ToolStripMenuItem("Manage indexed folders...");
            indexsettings.Click += show_indexsettings;
            ToolStripMenuItem additems = new ToolStripMenuItem("Add Items");
            additems.Text = "Add Items";
            additems.Click += Additems_Click;


            cm.MenuItems.Add("tags toevoegen", new EventHandler(tagstoevoegen));
            cm.MenuItems.Add("tags verwijderen", new EventHandler(tagsverwijderen));
            cm.MenuItems.Add("entries verwijderen", new EventHandler(entriesverwijderen));

            ToolStripMenuItem ImportMenu = new ToolStripMenuItem();
            ImportMenu.Text = "Import";
            ToolStripMenuItem ImportSingleFile_optie = new ToolStripMenuItem("Import Single File...");
            ImportSingleFile_optie.Click += importSingleFile;
            ToolStripMenuItem ImportMultipleFiles_optie = new ToolStripMenuItem("Import Multiple Files...");
            ImportMultipleFiles_optie.Click += importMultipleFiles;
            ToolStripMenuItem ImportEntireFolder_optie = new ToolStripMenuItem("Import Folder...");
            ImportEntireFolder_optie.Click += importFolder;

            ImportMenu.DropDownItems.Add(ImportSingleFile_optie);
            ImportMenu.DropDownItems.Add(ImportMultipleFiles_optie);
            ImportMenu.DropDownItems.Add(ImportEntireFolder_optie);
            filemenu.DropDownItems.Add(ImportMenu);

            searchicon.Size = new Size(20, 20);
            searchicon.Location = new Point(708, 30);
            searchicon.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            
            searchicon.Image = Properties.Resources.searchicoontje;
            
            searchbox.Size = new Size(700, 20);
            searchbox.Anchor = (AnchorStyles.Top |AnchorStyles.Left |AnchorStyles.Right);
            searchbox.Location = new Point(5, 30);
            searchbox.Text = "Search";
            searchbox.DropDownStyle = ComboBoxStyle.DropDown;
            searchbox.PreviewKeyDown += Searchbox_PreviewKeyDown;
            

            dataview.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);

            dataview.Size = new Size(700, 500);
            dataview.AutoSize = true;
            dataview.Location = new Point(5, 55);
     
            dataview.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            dataview.ScrollBars = ScrollBars.Vertical; //er moet data eerst getoond worden en dan komt automatisch de scrollbar 
            dataview.ReadOnly = true;
            dataview.RowHeadersVisible = false;
            dataview.AllowUserToAddRows = false;
            dataview.AllowUserToResizeRows = false;
            DataGridViewRow row = dataview.RowTemplate;
            row.MinimumHeight = 50;
            

            
            

           /* scrollbar1.Location = new Point(708, 55); //vragen  hoe je kan scrollen
            scrollbar1.Size = new Size(20, 500);
            scrollbar1.Minimum = 0;
            scrollbar1.Maximum = 500;
            scrollbar1.Value = 0; */

            this.Controls.Add(searchicon);
            this.Controls.Add(searchbox);
            this.Controls.Add(searchicon);
            this.Controls.Add(dataview);
          //  this.Controls.Add(scrollbar1);
            this.Controls.Add(menu);
            menu.Items.Add(filemenu);
            menu.Items.Add(editmenu);
            menu.Items.Add(dbmenu);
            menu.Items.Add(helpmenu);
            filemenu.DropDownItems.Add(export);
            filemenu.DropDownItems.Add(addfile);
            addfile.DropDownItems.Add(singlefile);
            addfile.DropDownItems.Add(addmulti);
            addfile.DropDownItems.Add(addfolder);
            filemenu.DropDownItems.Add(export);
            
            editmenu.DropDownItems.Add(delete);
            editmenu.DropDownItems.Add(selectall);
            dbmenu.DropDownItems.Add(view);
            dbmenu.DropDownItems.Add(addSingleItem_toolstrip);
            dbmenu.DropDownItems.Add(addMultipleItems_toolstrip);
            dbmenu.DropDownItems.Add(addEntireFolder);
            dbmenu.DropDownItems.Add(entryjump);
            dbmenu.DropDownItems.Add(indexsettings);
            //dbmenu.DropDownItems.Add(additems);


            searchicon.MouseClick += zoekfunctie;
            searchicon.MouseClick += searchengine;
            searchicon.MouseClick += datatogrid;
            dataview.CellDoubleClick += dataview_usedatafortagform;
            export.Click += Click_Export;

            entryjump.Click += entryjumpen;
            selectall.Click += selectallentries;
            dataview.CellMouseClick += optiemenu;

            if (!Directory.Exists(TaggyUserFolder))
                Directory.CreateDirectory(TaggyUserFolder);
            InitializeComponent();

        }

        private void show_indexsettings(object sender, EventArgs ea)
        {
            if (idf.IsDisposed)
                idf = new IndexDirectoriesSettings();
            this.idf.Show();
        }

        private void Searchbox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                e.IsInputKey = true;
        }

        private void Searchbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                datatogrid(sender, null);
                e.Handled = true;
                e.SuppressKeyPress = true;
                editmenu.Enabled = true;
        }
        }
        //private void datatogrid(object o, EventArgs e)
        //{
        //    dataview.DataSource = bindingsource1;
        //    addtodatagrid();
        //}

        // Adding files
        private void AddSingleItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectionDialog = new OpenFileDialog();
            DialogResult res = selectionDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                TaggyFile file = new TaggyFile(selectionDialog.FileName, this.conn);
                TaggyEntry entry = new TaggyEntry(file, this.conn);
                entry.Save();
                List<TaggyTag> taglist = TaggySearchEngine.VraagTags(this.conn,
                    true, default_input: "tagme");
                foreach (TaggyTag t in taglist)
                    entry.AddTag(t);
                new tagform(entry).Show();
            }

        }

        private void Additems_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectionDialog = new OpenFileDialog();
            DialogResult res = selectionDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                TaggyFile file = new TaggyFile(selectionDialog.FileName, this.conn);
                TaggyEntry entry = new TaggyEntry(file, this.conn);
                entry.Save();
                new tagform(entry).Show();
            }

        }



        //bind the datatable from the adddatatogrid method to the datagridview in the form
        private void datatogrid(object o, EventArgs e)
        {
            dataview.DataSource = bindingsource1;
            addtodatagrid();
            DataGridViewColumn column = dataview.Columns[2];
            column.Width = 50;
            dataview.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataview.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void addEntireFolder_Click (object o, EventArgs ea)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                List<TaggyTag> taglist = TaggySearchEngine.VraagTags(this.conn,
                    true, default_input: "tagme");
                foreach (string path in Directory.GetFiles(fbd.SelectedPath))
                {
                    TaggyFile file = new TaggyFile(path, this.conn);
                    TaggyEntry entry = new TaggyEntry(file, this.conn);
                    entry.Save();
                    foreach (TaggyTag t in taglist)
                        entry.AddTag(t);
                }
            }
        }
        private void addMultipleItems_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectionDialog = new OpenFileDialog();
            selectionDialog.Multiselect = true;
            if (selectionDialog.ShowDialog() == DialogResult.OK)
            {
                List<TaggyTag> taglist = TaggySearchEngine.VraagTags(this.conn, true,
                    "Voer de tags in die aan de bestanden moeten worden gekoppeld, gescheiden door spaties.",
                    "Tags toevoegen",
                    "tagme"
                    );
                if (taglist.Count == 0)
                {
                    MessageBox.Show("Er moeten tags worden verbonden aan de nieuwe bestanden!");
                }
                foreach (string f in selectionDialog.FileNames)
                {
                    TaggyFile file = new TaggyFile(f, this.conn);
                    TaggyEntry entry = new TaggyEntry(file, this.conn);
                    entry.Save();
                    foreach (TaggyTag tag in taglist)
                        entry.AddTag(tag);
                }
            }
        }

        // Importeren

        /// <summary>
        /// Importeert een los bestand naar Mijn documenten \ Taggy
        /// </summary>
        /// <param name="path"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        private TaggyEntry importFile(string path, List<TaggyTag> tags)
        {
            TaggyEntry te = null;
            try
            {
                string dest = Path.Combine(TaggyUserFolder, Path.GetFileName(path));
                File.Copy(path, dest);
                TaggyFile tf = new TaggyFile(dest, conn);
                te = new TaggyEntry(tf, conn);
                te.Save();
                foreach (TaggyTag t in tags)
                    te.AddTag(t);
            }
            catch (Exception e)
            {
                MessageBox.Show("Something broke while copying your file!");
                Console.WriteLine(e);
                MessageBox.Show(e.ToString());
            }
            return te;
            }
        private void importSingleFile(object o, EventArgs ea)
        {
            OpenFileDialog fsd = new OpenFileDialog();
            if (fsd.ShowDialog() != DialogResult.OK)
                return;
            List<TaggyTag> tags = TaggySearchEngine.VraagTags(conn, true, default_input:"tagme");
            TaggyEntry res = importFile(fsd.FileName, tags);
            if (res != null)
                MessageBox.Show("joepie het werkt");
        }

        private void importMultipleFiles(object o, EventArgs ea)
        {
            OpenFileDialog fsd = new OpenFileDialog();
            fsd.Multiselect = true;
            if (fsd.ShowDialog() != DialogResult.OK)
                return;
            List<TaggyTag> tags = TaggySearchEngine.VraagTags(conn, true, default_input:"tagme");
            foreach (string filename in fsd.FileNames)
                importFile(fsd.FileName, tags);
            MessageBox.Show("Successfully imported " + fsd.FileNames.Length.ToString() + " files.");
            }

        private void importFolder(object o, EventArgs ea)
        {
            FolderBrowserDialog fsd = new FolderBrowserDialog();
            if (fsd.ShowDialog() != DialogResult.OK)
                return;
            List<TaggyTag> tags = TaggySearchEngine.VraagTags(conn, true, default_input:"tagme");
            foreach (string f in Directory.EnumerateFiles(fsd.SelectedPath))
                importFile(f, tags);
        }
        // Eind importeren

        public void zoekfunctie(object o, EventArgs ea)
        {
            Tagzoekfunctie t = new Tagzoekfunctie(searchbox.Text);
            Console.WriteLine(t.Files);
            editmenu.Enabled = true;
        }

        public void searchengine(object sender, EventArgs e)
        {
            string result = "";
            TaggySearchEngine tse = new TaggySearchEngine(conn);
            foreach (Tuple<List<TaggyTag>, List<TaggyTag>> t in tse.ParseSearchQuery(searchbox.Text))
            {
                SQLiteCommand sqlc = (tse.MaakQuery(t));
                SQLiteDataReader tagreader = sqlc.ExecuteReader();
                while (tagreader.Read())
                {
                    result = tagreader["id"].ToString();
                    Console.WriteLine(result);
                }
            }
            editmenu.Enabled = true;
            
        }

        private void addtodatagrid(object o = null, EventArgs ea = null)
        {
            table = new DataTable();         
            table.Columns.Add("File ID");
            table.Columns.Add("File name");
            table.Columns.Add("Picture", typeof(Bitmap));
            Size imgsize = new Size(50, 50);
            HashSet<long> idSet = new HashSet<long>();
            long id;
            // SQLiteConnection conn = asdf
            // conn.Open()
            TaggySearchEngine tse = new TaggySearchEngine(conn);
            foreach (Tuple<List<TaggyTag>, List<TaggyTag>> t in tse.ParseSearchQuery(searchbox.Text))
            {
                SQLiteCommand sqlc = (tse.MaakQuery(t));
                SQLiteDataReader tagreader = sqlc.ExecuteReader();
                while (tagreader.Read())
                {
                    id = tagreader.GetInt64(0);
                    idSet.Add(id);
                }
                
                
            }
            
            //lees de ids uit de idset en vul de datagridview met de ids en bijbehorende imagepath.
            foreach (int i in idSet)
            {
                TaggyEntry te = new TaggyEntry(i, conn);                
                string idrow = te.ID.ToString();
                string filenamerow = Path.GetFileName(te.File.Path).ToString();
                Bitmap img = resizeBitmap((Bitmap) te.File.Thumbnail, imgsize);
                table.Rows.Add(idrow, filenamerow, img);
                
            }
            
            bindingsource1.DataSource = table;
            dataview.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            
        }

        public static Bitmap resizeBitmap (Bitmap thumbnail, Size size)
        {
            try
            {
                Bitmap newpic = new Bitmap(size.Width, size.Height);
                using (Graphics g = Graphics.FromImage((Image)newpic))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(thumbnail, 0, 0, size.Width, size.Height);
                }
                return newpic;
            }
            catch
            {
                return thumbnail;
            }                      
        }

        private void entryjumpen(object o, EventArgs ea)
        {
            long entrygetal;
            string entry = Interaction.InputBox("vul een entry-id in", "Jump to entry ...", "1", 100, 100);
            if (Int64.TryParse(entry, out entrygetal))
            {
                TaggyEntry entryl = new TaggyEntry(entrygetal, conn);
                tagform tf = new tagform(entryl);
                tf.Show();
            }
            else
            {
                MessageBox.Show("You can only fill in an existing entrynumber, not a non-number", "Error");
            }
        }
            
        public void selectallentries(object o, EventArgs ea)
        {
            dataview.SelectAll();
        }

        private void dataview_usedatafortagform(object sender, EventArgs e)
        {
            long idfromrow;
            string idstring = dataview[0, dataview.CurrentRow.Index].Value.ToString();
            bool succes = Int64.TryParse(idstring, out idfromrow);

            if (succes)
            {
                TaggyEntry TE = new TaggyEntry(idfromrow, conn);
                tagform tagf = new tagform(TE);
                tagf.Show();
            }
        }

        private void optiemenu(object o, MouseEventArgs mea) //met rechtermuisknop op het bestand wordt er een optiemenu geopend.
        {
            if (mea.Button == MouseButtons.Right)
            {
                dataview.ContextMenu = cm;
                dataview.ContextMenu.Show(dataview, mea.Location);
            }
            }

        private void tagstoevoegen(object o, EventArgs ea) //voegt de tags aan het bestand toe die de gebruiker heeft ingevoerd
        {
            DataGridViewSelectedRowCollection rows;
            rows = dataview.SelectedRows;
            List<long> ids = new List<long>();
     
            foreach (DataGridViewRow row in rows)
            {
                ids.Add(long.Parse(row.Cells[0].Value.ToString()));
            }
            List<TaggyTag> taglist = TaggySearchEngine.VraagTags(conn,
                true, default_input: "tagme");
            foreach (long id in ids)
            {
                TaggyEntry e = new TaggyEntry(id, conn);
                foreach(TaggyTag t in taglist)
                {
                    e.AddTag(t);
                }
            }
        }

        private void tagsverwijderen(object o, EventArgs ea) //de aangeroepen klasse opent een form waarin de gebruiker tags kan selecteren en verwijderen
        {
            DataGridViewSelectedRowCollection rows;
            rows = dataview.SelectedRows;
            remove_tagsform form = new remove_tagsform(rows, dataview, conn);
            form.Show();
            form.FormClosed += addtodatagrid;
        }

        private void entriesverwijderen(object o, EventArgs ea)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("Are you sure you want to delete these entries?", "warning", buttons);
            if (result == DialogResult.Yes)
            {
                DataGridViewSelectedRowCollection rows;
                rows = dataview.SelectedRows;
                List<long> ids = new List<long>();

                foreach (DataGridViewRow row in rows)
                {
                    ids.Add(long.Parse(row.Cells[0].Value.ToString()));
                }
                foreach (long id in ids)
                {
                    TaggyEntry e = new TaggyEntry(id, conn);
                    e.Delete();
                }       
            }
            addtodatagrid();
        }

        private void helpoverview(object o, EventArgs ea)
        {
            Form help = new Form();
            Button ok = new Button();
            Label text = new Label();
            text.Text = "You can search for entries by typing tag(s) in the searchbar and press enter or click the searchicon. The results of your searchcommand will be displayed below the searchbar." + Environment.NewLine +
            Environment.NewLine + "By selecting one or more entries and rightclick on them, you can add or remove tags from the entry or entries you selected and you van remove the entry or entries itself." + Environment.NewLine +
            Environment.NewLine + "By doubleclicking an entry you will go to another screen where you can easely add or remove tags to/from this entry." + Environment.NewLine +
            Environment.NewLine + "Under the tab file, you can import a file, multiple files or even a folder to this application and start adding tags to it. You can also export one or multiple searchresults to a prefered folder." + Environment.NewLine +
            Environment.NewLine + "Under the tab Edit, you van select all files that are displayed under the searchbar and with delete entry you can delete the selected files you see under the searchbar. This function is only available when there are searchresults displayed." + Environment.NewLine +
            Environment.NewLine + "Under the tab Database Maintenance you can also import a file, multiple files or even a folder to this application. With jump to entry... you can typ in the entrynumber of a file and start adding or removing tags from that file.";
            text.Location = new Point(10, 10);
            text.Size = new Size(470, 270);
            ok.Location = new Point(210, 280);
            ok.Text = "ok";
            ok.DialogResult = DialogResult.OK;
            help.Size = new Size(500, 350);
            help.Text = "Help";
            help.FormBorderStyle = FormBorderStyle.FixedDialog;
            help.AcceptButton = ok;
            help.StartPosition = FormStartPosition.CenterParent;
            help.MaximizeBox = false;
            help.Controls.Add(text);
            help.Controls.Add(ok);
            help.ShowDialog();

            if (help.DialogResult == DialogResult.OK)
            {
                return;
            }
        }

        private void Click_Export(object o, EventArgs ea)
        {
            if (searchbox.Text != "Search")
            {   
                
                try
                {
                    FolderBrowserDialog brd = new FolderBrowserDialog();
                    if (brd.ShowDialog() == DialogResult.OK)
                    {
                        string foldername = brd.SelectedPath;
                        foreach (DataGridViewRow row in dataview.SelectedRows)
                        {
                              Console.WriteLine(dataview.SelectedRows);
                              long id = long.Parse(dataview.SelectedRows[0].Cells[0].Value.ToString());
                              TaggyEntry e = new TaggyEntry(id, conn);
                              var name = Path.GetFileName(e.File.Path);
                              var path = Path.Combine(foldername, name);
                              var source = e.File.Path;
                              File.Copy(source, path);
                              MessageBox.Show("File(s) are saved in the selected folder");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("This file(s) is already saved in the selected folder");
                }
        }
            else
            {
                MessageBox.Show("No files are selected");
            }
           // FolderBrowserDialog fbd = new FolderBrowserDialog();
            
             //   foreach (int i in dataview.Rows)
               // {
                   
              //  }
            

        }

      /*  public void scrollmethode(object o, ScrollEventArgs sc)
        {
           
        } */


    }
}
