﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using Taggy.Classes;

namespace Taggy
{
    public partial class removetagsform : Form //form wanneer er rechstgeklikt wordt op zoekresultaten en dan op remove tags.
    {
        DataGridViewSelectedRowCollection rows;
        ListBox tagbox = new ListBox();
        SQLiteConnection conn = new SQLiteConnection($"Data Source = {Properties.Settings.Default["DatabasePath"]}");
        DataGridView dataview;
        HashSet<TaggyTag> taglist = new HashSet<TaggyTag>(new TaggyEntityComparer());
        List<long> ids = new List<long>();

        /// <summary>
        /// Dit is de consttructormethode van deze klasse, hier wordt alles voor op de formdesign geplaatst en de benodigde informatie in de juiste variabelen geplaatst.
        /// </summary>
        /// <param name="DGVSRC"></param>
        /// <param name="DVG"></param>
        /// <param name="SQLC"></param>
        public removetagsform(DataGridViewSelectedRowCollection DGVSRC, DataGridView DVG, SQLiteConnection SQLC)
        {
            conn = SQLC;
            rows = DGVSRC;
            dataview = DVG;

            Size = new Size(250, 250);
            MaximizeBox = false;

            Label explanation = new Label();
            Button ok = new Button();
            Button cancel = new Button();
            
            foreach (DataGridViewRow row in rows)
            {
                ids.Add(long.Parse(row.Cells[0].Value.ToString()));
            }

            explanation.Text = "Select the tags you want to remove";
            explanation.Location = new Point(10, 10);
            explanation.Size = new Size(250, 18);

            tagbox.Location = new Point(10, 30);
            tagbox.Size = new Size(220, 140);
            tagbox.ScrollAlwaysVisible = true;
            tagbox.SelectionMode = SelectionMode.MultiExtended;

            foreach (long id in ids)
            {
                TaggyEntry e = new TaggyEntry(id, conn);
                List<TaggyTag> tags = e.GetTags();
                foreach (TaggyTag t in tags)
                {
                    taglist.Add(t);
                }
            }

            foreach (TaggyTag t in taglist)
            {
                tagbox.Items.Add(t);
            }

            ok.Text = "OK";
            ok.Location = new Point(10, 180);
            ok.Size = new Size(30, 20);

            cancel.Text = "Cancel";
            cancel.Location = new Point(80, 180);
            cancel.Size = new Size(100, 20);

            Controls.Add(explanation);
            Controls.Add(tagbox);
            Controls.Add(ok);
            Controls.Add(cancel);

            InitializeComponent();

            ok.Click += okclicked;
            cancel.Click += cancelclicked;
        }

        /// <summary>
        /// Als er op ok geklikt wordt, dan worden de geselecteerde tags daadwerkelijk verwijderd.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void okclicked(object o, EventArgs ea)
        {
            if (tagbox.SelectedItem == null)
                Close();

            List<TaggyTag> taggytaglist = new List<TaggyTag>();

            foreach (TaggyTag t in tagbox.SelectedItems)
            {
                taggytaglist.Add(t);
            }

            foreach (long id in ids)
            {
                TaggyEntry entry = new TaggyEntry(id, conn);
                foreach (TaggyTag t in taggytaglist)
                {
                    entry.RemoveTag(t);
                    tagbox.Items.Remove(t);
                    tagbox.Refresh();
                }
            }
            Close();
        }

        /// <summary>
        /// Als er op cancel wordt geklikt wordt de hele operatie gestopt en keert de gebruiker terug naar taggysearchform.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void cancelclicked(object o, EventArgs ea)
        {
            Close();
        }
    }
}
