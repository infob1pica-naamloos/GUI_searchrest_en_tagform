﻿using System.Drawing;
using System.Windows.Forms;
using Taggy.Classes;
using System.IO;
using System;

namespace Taggy
{
    public partial class tagentryform : Form
    {
        public bool EntryDeleted = false;   // Wordt gecontroleerd om te zien of de zoekresultaten ververst moeten worden
        Label path, entryid, lastchanged, date;
        ListBox tagbox;
        PictureBox file_display;
        TaggyEntry entry;

        /// <summary>
        /// Dit is de constructormethode van deze klasse, hierin wordt alles voor op de formdesign geplaatst en de juiste informatie klaargezet in variabelen.
        /// </summary>
        /// <param name="TE"></param>
        public tagentryform(TaggyEntry TE)
        {
            this.Icon = Properties.Resources.icoon;
            entry = TE;
            Text = "Taggy";
            ClientSize = new Size(800, 600);
            KeyPreview = true;
            MinimumSize = new Size(600, 450);

            MenuStrip menu;
            Button addtag_button, deletetag_button;
            GroupBox displayinfo;
            Label path_label, entryid_label, lastchtext, datetext;
            ContextMenu displaycontext;

            file_display = new PictureBox();          
            addtag_button = new Button();
            deletetag_button = new Button();          
            menu = new MenuStrip();
            tagbox = new ListBox();
            displayinfo = new GroupBox();
            path_label = new Label();       //tekst die voor resultaat van label staat
            entryid_label = new Label();    //tekst die voor resultaat van label staat
            path = new Label();             //label waar data van DB komt te staan
            entryid = new Label();
            lastchtext = new Label();       //label waar data van DB komt te staan
            lastchanged = new Label();
            date = new Label();
            datetext = new Label();
            displaycontext = new ContextMenu();
            
            menu.Size = new Size(672, 25);
            menu.Dock = DockStyle.Top;
            ToolStripMenuItem editmenu = new ToolStripMenuItem("Edit");
            editmenu.Text = "Edit";
            
            ToolStripMenuItem helpmenu = new ToolStripMenuItem("Help");
            helpmenu.Text = "Help";
            helpmenu.Click += helpoverview;

            ToolStripMenuItem delete_entry = new ToolStripMenuItem("Delete Entry");
            ToolStripMenuItem delete_tag = new ToolStripMenuItem("Delete Tag");
            delete_tag.Text = "Delete Tag";

            ToolStripMenuItem selectall = new ToolStripMenuItem("Select All");
            selectall.Text = "Select All Entries";

            addtag_button.Size = new Size(75, 45);
            addtag_button.Anchor = (AnchorStyles.Right | AnchorStyles.Top);
            addtag_button.Location = new Point(578, 55);
            addtag_button.Text = "Add";
            addtag_button.Image = Properties.Resources.tagiconnn;
            addtag_button.ImageAlign = ContentAlignment.MiddleLeft;
            addtag_button.Click += addtagsbutton_click;

            deletetag_button.Size = new Size(75, 45);
            deletetag_button.Location = new Point(497, 55);
            deletetag_button.Anchor = (AnchorStyles.Right | AnchorStyles.Top);
            deletetag_button.Text = "  Delete";
            deletetag_button.Image = Properties.Resources.trashbin;
            deletetag_button.ImageAlign = ContentAlignment.MiddleLeft;

            file_display.Location = new Point(5, 115);
            file_display.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            file_display.Size = new Size(650, 313);          
            file_display.BackColor = Color.White;
            file_display.SizeMode = PictureBoxSizeMode.CenterImage;
            file_display.BorderStyle = BorderStyle.Fixed3D;
            file_display.ContextMenu = displaycontext;

            MenuItem displaycontextitem1 = new MenuItem("Open");
            MenuItem displaycontextitem2 = new MenuItem("Open with");

            tagbox.Location = new Point(660, 115);
            tagbox.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Right);
            tagbox.Size = new Size(120, 450);

            displayinfo.Dock = DockStyle.Bottom;
            displayinfo.Size = new Size(100, 140);

            path_label.Text = "Path: ";
            path_label.Location = new Point(10, 30);
            path_label.AutoSize = true;
            
            path.Location = new Point(100, 30);
            path.AutoSize = true;
            
            entryid.Location = new Point(100, 60);
            entryid.AutoSize = true;

            lastchanged.Location = new Point(100, 90);
            lastchanged.AutoSize = true;

            date.Location = new Point(100, 120);
            date.AutoSize = true;
            
            entryid_label.Text = "Id: ";
            entryid_label.Location = new Point(10, 60);
            entryid_label.AutoSize = true;

            lastchtext.Text = "Last Changed: ";
            lastchtext.Location = new Point(10, 90);
            lastchtext.AutoSize = true;

            datetext.Text = "Date and Time: ";
            datetext.Location = new Point(10, 120);
            datetext.AutoSize = true;
            
            Controls.Add(menu);
            Controls.Add(addtag_button);
            Controls.Add(deletetag_button);            
            Controls.Add(file_display);
            Controls.Add(tagbox);
            Controls.Add(displayinfo);
            
            displayinfo.Controls.Add(path_label);
            displayinfo.Controls.Add(entryid_label);
            displayinfo.Controls.Add(lastchtext);
            displayinfo.Controls.Add(path);
            displayinfo.Controls.Add(entryid);
            displayinfo.Controls.Add(lastchanged);
            displayinfo.Controls.Add(date);
            displayinfo.Controls.Add(datetext);
            
            menu.Items.Add(editmenu);
            menu.Items.Add(helpmenu);

            editmenu.DropDownItems.Add(delete_entry);
            editmenu.DropDownItems.Add(delete_tag);

            displaycontext.MenuItems.Add(displaycontextitem1);
            displaycontext.MenuItems.Add(displaycontextitem2);

            InitializeComponent();

            tagform_Load(null, null);

            delete_tag.Click += deletetag;
            deletetag_button.Click += deletetag;
            delete_entry.Click += delentry;
            displaycontextitem1.Click += loadwstrdeditor;
            displaycontextitem2.Click += showopenwithdialog;
        }

        /// <summary>
        /// Deze methode zet de juiste informatie voor de tagentryform erin, zodat deze geladen kan worden met de juiste entry.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tagform_Load(object sender, EventArgs e)
        {
            path.Text = entry.File.Path;
            entryid.Text = entry.ID.ToString();           
            
            try
            {
                using (Image img = Image.FromFile(this.entry.File.Path))
                {
                    int x = img.Width;
                    int y = img.Height;
                if (x > file_display.Width)
                    file_display.SizeMode = PictureBoxSizeMode.Zoom;
                if (y > file_display.Height)
                    file_display.SizeMode = PictureBoxSizeMode.Zoom;
                    file_display.Image = new Bitmap(img);
                    img.Dispose();
                }
            }
                catch
                {
                file_display.Image = Properties.Resources.rederror;
                this.entry.File.MarkBroken();
            }
            
            if (entry.LastChanged == null) //lastchanged datum van file weergeven in label
            {
                lastchanged.Text = "Not found";
            }
            else
            {
                lastchanged.Text = entry.LastChanged.ToString();
            }

            date.Text = entry.Added.ToString(); //datum en tijd van file

            foreach (TaggyTag t in entry.GetTags()) //lijst van tags weergeven
            {
                tagbox.Items.Add(t);
            }
        } 

        /// <summary>
        /// Opent het standaard programma voor het juiste soort bestand als de gebruiker rechtsklikt op de entry en dan op Open klikt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadwstrdeditor(object sender, EventArgs e)
        {
            System.Diagnostics.ProcessStartInfo startinfo = new System.Diagnostics.ProcessStartInfo(this.entry.File.Path);
            startinfo.Verb = "edit";

            System.Diagnostics.Process.Start(startinfo);
            Close();
        }

        /// <summary>
        /// Opent een venster waar de gebruiker kan kiezen uit verschillende programma's waarmee het bestand kan worden geopend.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showopenwithdialog(object sender, EventArgs e)
        {
            string args = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "shell32.dll");
            args += ",OpenAs_RunDLL " + entry.File.Path;
            System.Diagnostics.Process.Start("rundll32.exe", args);
        }

        /// <summary>
        /// Deze methode wordt gebruikt als je een tag wilt verwijderen van een entry.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ea"></param>
        private void deletetag(object sender, EventArgs ea)
        {
            if (tagbox.SelectedItem == null)
            {
                MessageBox.Show("You have not selected a tag");
                return;
            }
            TaggyTag t = (TaggyTag)tagbox.SelectedItem;
            entry.RemoveTag(t);
            lastchanged.Text = entry.LastChanged.ToString();
            tagbox.Items.Remove(t);
            tagbox.Refresh();
            tagform_Load(null, null);
        }

        /// <summary>
        /// Deze methode wordt gebruikt als je de gehele entry uit Taggy wilt verwijderen.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void delentry(object o, EventArgs ea)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            string text = "Are you sure you want to delete this entry?";
            string caption = "Delete this file";
            DialogResult message;
            string text2 = "Do you want to delete this file from your harddisk?";
            string text3 = "Can not delete the file at the moment.";
            message = MessageBox.Show(text, caption, buttons);
            if (message == DialogResult.Yes)
            {
                long oldid = entry.ID;
                entry.Delete();
                this.EntryDeleted = true;
                message = MessageBox.Show(text2, caption, buttons);
                if (message == DialogResult.Yes)
                {
                    try
                    {
                        File.Delete(entry.File.Path);
                        File.Delete(entry.File.ThumbnailPath);
                        Close();
                    }
                    catch(IOException)
                    {
                        message = MessageBox.Show(text3, caption);
                    }
                }
                else
                {
                    string TaggyUserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Taggy");
                    if (entry.File.Path.StartsWith(TaggyUserFolder))
                    {
                        string newfilename = Path.Combine(
                            Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                            $"Taggy_removed_{oldid}" + Path.GetFileName(entry.File.Path)
                            );
                        File.Move(entry.File.Path, newfilename);
                        MessageBox.Show($"Your file was moved to your 'Documents' folder, because it was an imported file. The new location is: {newfilename}.");
                    }
                }
                Close();
            }
        }

        /// <summary>
        /// Deze methode wordt gebruikt als je tag(s) wilt toevoegen aan de entry.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void addtagsbutton_click(object o, EventArgs ea)
        {
            var tags = TaggySearchEngine.VraagTags(entry.connection, true);

            foreach (TaggyTag t in tags)
            {
                tagbox.Items.Add(t);
                entry.AddTag(t);
            }

            lastchanged.Text = entry.LastChanged.ToString();
            Refresh();
        }
       
        /// <summary>
        /// Deze methode zorgt ervoor dat er een helpscherm wordt geopend als je op help klikt.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void helpoverview(object o, EventArgs ea)
        {
            Form help = new Form();
            Button ok = new Button();
            Label text = new Label();
            text.Text = "In this window you can add or remove tags to the entry you have selected, and view some information about this entry. At the right-hand side of the screen you will see all the tags your entry has. By selecting a tag and press the 'Delete' button, you will remove that tag from this entry. By pressing the add button you can add tags to this entry." + Environment.NewLine +
            Environment.NewLine + "In the Edit menu, you can delete the file you are looking at by clicking delete entry and if you have selected a tag you can click delete tag to delete this tag." + Environment.NewLine +
            Environment.NewLine + "For more information see our instructions.";
            text.Location = new Point(10, 10);
            text.Size = new Size(470, 120);
            ok.Location = new Point(200, 140);
            ok.Text = "ok";
            ok.DialogResult = DialogResult.OK;
            help.Size = new Size(500, 210);
            help.Text = "Help";
            help.FormBorderStyle = FormBorderStyle.FixedDialog;
            help.AcceptButton = ok;
            help.StartPosition = FormStartPosition.CenterParent;
            help.MaximizeBox = false;
            help.MinimizeBox = false;
            help.Controls.Add(text);
            help.Controls.Add(ok);
            help.ShowDialog();

            if (help.DialogResult == DialogResult.OK)
            {
                return;
            }
        }
    }
}
