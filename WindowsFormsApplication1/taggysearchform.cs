﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Collections.Generic;
using Taggy.Classes;
using Microsoft.VisualBasic;
using System.Data;

namespace Taggy
{
    public partial class taggysearchform : Form
    {
        DataTable table = new DataTable();
        ComboBox searchbox;
        SQLiteConnection conn = new SQLiteConnection($"Data Source = {Taggy.Properties.Settings.Default["DatabasePath"]}");
        ToolStripMenuItem editmenu;
        string TaggyUserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Taggy");
        BindingSource bindingsource1;
        DataGridView dataview;
        ContextMenu cm;
        IndexDirectoriesSettings idf;
        List<string> alltags;

        public taggysearchform()
        {
            this.Icon = Properties.Resources.icoon;
            conn.Open();
            idf = new IndexDirectoriesSettings();
            idf.PerformIndexSearch();
            Text = "Taggy";
            ClientSize = new Size(800, 600);
            MinimumSize = new Size(600, 450);
           
            PictureBox searchicon;
            MenuStrip menu;

            bindingsource1 = new BindingSource();
            searchicon = new PictureBox();
            searchbox = new ComboBox();
            dataview = new DataGridView();
            menu = new MenuStrip();
            cm = new ContextMenu();

            menu.Size = new Size(672, 25);
            menu.Dock = DockStyle.Top;
           
            ToolStripMenuItem filemenu = new ToolStripMenuItem("File");
            filemenu.Text = "File";
            editmenu = new ToolStripMenuItem("Edit");
            editmenu.Text = "Edit";
            editmenu.Enabled = false;
            ToolStripMenuItem dbmenu = new ToolStripMenuItem("Database Maintenance");
            dbmenu.Text = "Database Maintenance";
            ToolStripMenuItem helpmenu = new ToolStripMenuItem("Help");
            helpmenu.Text = "Help";
            helpmenu.Click += helpoverview;

            ToolStripMenuItem addfile = new ToolStripMenuItem("Add Files");
            addfile.Text = "Add Files";
            ToolStripMenuItem singlefile = new ToolStripMenuItem("Add Single File");
            singlefile.Text = "Add Single File";
            singlefile.Click += AddSingleItem_Click;
            ToolStripMenuItem addmulti = new ToolStripMenuItem("Add Multiple Files");
            addmulti.Text = "Add Multiple Files";
            addmulti.Click += addMultipleItems_Click;
            ToolStripMenuItem addfolder = new ToolStripMenuItem("Add Folder");
            addfolder.Text = "Add Folder";
            addfolder.Click += addEntireFolder_Click;

            ToolStripMenuItem export = new ToolStripMenuItem("Export");
            export.Text = "Export";
            
            ToolStripMenuItem delete = new ToolStripMenuItem("Delete Entry/Entries");
            delete.Text = "Delete Entry";
            delete.Click += remove_entries;
            
            ToolStripMenuItem selectall = new ToolStripMenuItem("Select All Entries");
            selectall.Text = "Select All Entries";

            ToolStripMenuItem entryjump = new ToolStripMenuItem("Jump to entry ...");
            entryjump.Text = "Jump to entry ...";
            ToolStripMenuItem indexsettings = new ToolStripMenuItem("Manage indexed folders...");
            indexsettings.Click += show_indexsettings;
            ToolStripMenuItem additems = new ToolStripMenuItem("Add Items");
            additems.Text = "Add Items";
            additems.Click += Additems_Click;

            ToolStripMenuItem ImportMenu = new ToolStripMenuItem();
            ImportMenu.Text = "Import";
            ToolStripMenuItem ImportSingleFile_optie = new ToolStripMenuItem("Import Single File...");
            ImportSingleFile_optie.Click += importSingleFile;
            ToolStripMenuItem ImportMultipleFiles_optie = new ToolStripMenuItem("Import Multiple Files...");
            ImportMultipleFiles_optie.Click += importMultipleFiles;
            ToolStripMenuItem ImportEntireFolder_optie = new ToolStripMenuItem("Import Folder...");
            ImportEntireFolder_optie.Click += importFolder;

            searchicon.Size = new Size(20, 20);
            searchicon.Location = new Point(708, 30);
            searchicon.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            searchicon.Image = Properties.Resources.searchicoontje;
            
            searchbox.Size = new Size(700, 20);
            searchbox.Anchor = (AnchorStyles.Top |AnchorStyles.Left |AnchorStyles.Right);
            searchbox.Location = new Point(5, 30);
            searchbox.Text = "Search";
            searchbox.DropDownStyle = ComboBoxStyle.DropDown;
            searchbox.PreviewKeyDown += Searchbox_PreviewKeyDown;
            
            dataview.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            dataview.Size = new Size(700, 500);
            dataview.AutoSize = true;
            dataview.Location = new Point(5, 55);
            dataview.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataview.ScrollBars = ScrollBars.Vertical; //er moet data eerst getoond worden en dan komt automatisch de scrollbar 
            dataview.ReadOnly = true;
            dataview.RowHeadersVisible = false;
            dataview.AllowUserToAddRows = false;
            dataview.AllowUserToResizeRows = false;

            DataGridViewRow row = dataview.RowTemplate;
            row.MinimumHeight = 50;
            
            cm.MenuItems.Add("Add tag(s)", new EventHandler(add_tags));
            cm.MenuItems.Add("Remove tag(s)", new EventHandler(remove_tags));
            cm.MenuItems.Add("Remove entry or entries", new EventHandler(remove_entries));

            Controls.Add(searchicon);
            Controls.Add(searchbox);
            Controls.Add(searchicon);
            Controls.Add(dataview);
            Controls.Add(menu);

            menu.Items.Add(filemenu);
            menu.Items.Add(editmenu);
            menu.Items.Add(dbmenu);
            menu.Items.Add(helpmenu);

            filemenu.DropDownItems.Add(addfile);
            filemenu.DropDownItems.Add(ImportMenu);
            filemenu.DropDownItems.Add(export);

            addfile.DropDownItems.Add(singlefile);
            addfile.DropDownItems.Add(addmulti);
            addfile.DropDownItems.Add(addfolder);

            ImportMenu.DropDownItems.Add(ImportSingleFile_optie);
            ImportMenu.DropDownItems.Add(ImportMultipleFiles_optie);
            ImportMenu.DropDownItems.Add(ImportEntireFolder_optie);
            
            editmenu.DropDownItems.Add(delete);
            editmenu.DropDownItems.Add(selectall);

            dbmenu.DropDownItems.Add(entryjump);
            dbmenu.DropDownItems.Add(indexsettings);

            taggysearch_shown();

            searchicon.MouseClick += datatogrid;
            dataview.CellDoubleClick += dataview_usedatafortagform;
            export.Click += Click_Export;
            entryjump.Click += entryjumpen;
            selectall.Click += selectallentries;
            dataview.CellMouseClick += optionmenu;
            searchbox.KeyDown += Searchbox_KeyDown;

            if (!Directory.Exists(TaggyUserFolder))
                Directory.CreateDirectory(TaggyUserFolder);
            //een stuk code dat de combobox searchbox je zoekterm laat afmaken en
            //dat ervoor zorgt dat de dropdownlist werkt.
            searchbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection tags = new AutoCompleteStringCollection();
            tags.Add("Search");
            alltags.Sort();
            foreach (string s in alltags)
            {
                tags.Add(s);
            }

            searchbox.DataSource = tags;
            searchbox.AutoCompleteCustomSource = tags;
            searchbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            InitializeComponent();

        }
        /// <summary>
        /// vult een list met alle tags die er op dat moment zijn (is nodig voor de autocomplete van de combobox)
        /// </summary>
        private void taggysearch_shown()
        {
            string query = "SELECT name FROM tags WHERE is_alias = 0";
            string tagname = "";
            alltags = new List<string>();
            SQLiteCommand sqlc = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = sqlc.ExecuteReader();

            while (reader.Read())
            {
                tagname = reader["name"].ToString();
                alltags.Add(tagname);
            }
        }

        private void show_indexsettings(object sender, EventArgs ea)
        {
            if (idf.IsDisposed)
                idf = new IndexDirectoriesSettings();
            idf.Show();
        }

        private void Searchbox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                e.IsInputKey = true;
        }

        private void Searchbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                datatogrid(sender, null);
                e.Handled = true;
                e.SuppressKeyPress = true;
                editmenu.Enabled = true;
        }
        }

        // Adding files
        private void AddSingleItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectionDialog = new OpenFileDialog();
            DialogResult res = selectionDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                TaggyFile file = new TaggyFile(selectionDialog.FileName, this.conn);
                TaggyEntry entry = new TaggyEntry(file, this.conn);
                entry.Save();
                List<TaggyTag> taglist = TaggySearchEngine.VraagTags(this.conn,
                    true, default_input: "tagme");
                foreach (TaggyTag t in taglist)
                    entry.AddTag(t);
                new tagentryform(entry).Show();
            }
        }

        private void Additems_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectionDialog = new OpenFileDialog();
            DialogResult res = selectionDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                TaggyFile file = new TaggyFile(selectionDialog.FileName, this.conn);
                TaggyEntry entry = new TaggyEntry(file, this.conn);
                entry.Save();
                new tagentryform(entry).Show();
            }
        }

        /// <summary>
        /// Laadt de resultaten in de datagridview en geeft ze weer
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void datatogrid(object o, EventArgs e)
        {
            dataview.DataSource = bindingsource1;
            addtodatagrid();
            DataGridViewColumn column = dataview.Columns[2];
            column.Width = 50;
            dataview.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataview.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void addEntireFolder_Click (object o, EventArgs ea)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                List<TaggyTag> taglist = TaggySearchEngine.VraagTags(this.conn,
                    true, default_input: "tagme");
                foreach (string path in Directory.GetFiles(fbd.SelectedPath))
                {
                    TaggyFile file = new TaggyFile(path, this.conn);
                    TaggyEntry entry = new TaggyEntry(file, this.conn);
                    entry.Save();
                    foreach (TaggyTag t in taglist)
                        entry.AddTag(t);
                }
            }
        }

        private void addMultipleItems_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectionDialog = new OpenFileDialog();
            selectionDialog.Multiselect = true;

            if (selectionDialog.ShowDialog() == DialogResult.OK)
            {
                List<TaggyTag> taglist = TaggySearchEngine.VraagTags(this.conn, true,
                    "Voer de tags in die aan de bestanden moeten worden gekoppeld, gescheiden door spaties.",
                    "Tags toevoegen",
                    "tagme"
                    );
                if (taglist.Count == 0)
                {
                    MessageBox.Show("Er moeten tags worden verbonden aan de nieuwe bestanden!");
                }
                foreach (string f in selectionDialog.FileNames)
                {
                    TaggyFile file = new TaggyFile(f, this.conn);
                    TaggyEntry entry = new TaggyEntry(file, this.conn);
                    entry.Save();
                    foreach (TaggyTag tag in taglist)
                        entry.AddTag(tag);
                }
            }
        }

        // Importeren
        /// <summary>
        /// Importeert een los bestand naar Mijn documenten \ Taggy
        /// </summary>
        /// <param name="path"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        private TaggyEntry importFile(string path, List<TaggyTag> tags)
        {
            TaggyEntry te = null;

            try
            {
                string dest = Path.Combine(TaggyUserFolder, Path.GetFileName(path));
                File.Copy(path, dest);
                TaggyFile tf = new TaggyFile(dest, conn);
                te = new TaggyEntry(tf, conn);
                te.Save();
                foreach (TaggyTag t in tags)
                    te.AddTag(t);
            }
            catch 
            {
                MessageBox.Show($"The file {path} is already present or could not be copied");
            }
            return te;
            }

        private void importSingleFile(object o, EventArgs ea)
        {
            OpenFileDialog fsd = new OpenFileDialog();

            if (fsd.ShowDialog() != DialogResult.OK)
                return;
            List<TaggyTag> tags = TaggySearchEngine.VraagTags(conn, true, default_input:"tagme");
            TaggyEntry res = importFile(fsd.FileName, tags);
        }

        private void importMultipleFiles(object o, EventArgs ea)
        {
            OpenFileDialog fsd = new OpenFileDialog();
            fsd.Multiselect = true;

            if (fsd.ShowDialog() != DialogResult.OK)
                return;

            List<TaggyTag> tags = TaggySearchEngine.VraagTags(conn, true, default_input:"tagme");

            foreach (string filename in fsd.FileNames)
            {
                importFile(filename, tags);
            }
            MessageBox.Show("Successfully imported " + fsd.FileNames.Length.ToString() + " files.");
            }

        private void importFolder(object o, EventArgs ea)
        {
            FolderBrowserDialog fsd = new FolderBrowserDialog();

            if (fsd.ShowDialog() != DialogResult.OK)
                return;

            List<TaggyTag> tags = TaggySearchEngine.VraagTags(conn, true, default_input:"tagme");

            foreach (string f in Directory.EnumerateFiles(fsd.SelectedPath))
                importFile(f, tags);
        }
        // Eind importeren

        /// <summary>
        /// Verwerkt de ingevoerde tekst 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void searchengine(object sender, EventArgs e)
        {
            string result = "";
            TaggySearchEngine tse = new TaggySearchEngine(conn);

            foreach (Tuple<List<TaggyTag>, List<TaggyTag>> t in tse.ParseSearchQuery(searchbox.Text))
            {
                SQLiteCommand sqlc = (tse.MaakQuery(t));
                SQLiteDataReader tagreader = sqlc.ExecuteReader();
                while (tagreader.Read())
                {
                    result = tagreader["id"].ToString();
                    Console.WriteLine(result);
                }
            }
            
        }

        /// <summary>
        /// Verwerk de zoekopdracht in de zoekbalk en haal de resultaten op.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void addtodatagrid(object o = null, EventArgs ea = null)
        {
            table = new DataTable();         
            table.Columns.Add("File ID");
            table.Columns.Add("File name");
            table.Columns.Add("Picture", typeof(Bitmap));
            Size imgsize = new Size(50, 50);
            HashSet<long> idSet = new HashSet<long>();
            long id;
            TaggySearchEngine tse = new TaggySearchEngine(conn);

            foreach (Tuple<List<TaggyTag>, List<TaggyTag>> t in tse.ParseSearchQuery(searchbox.Text))
            {
                SQLiteCommand sqlc = (tse.MaakQuery(t));
                SQLiteDataReader tagreader = sqlc.ExecuteReader();
                while (tagreader.Read())
                {
                    id = tagreader.GetInt64(0);
                    idSet.Add(id);
                }
            editmenu.Enabled = true;
            }
            
            //lees de ids uit de idset en vul de datagridview met de ids en bijbehorende imagepath.
            foreach (int i in idSet)
            {
                TaggyEntry te = new TaggyEntry(i, conn);                
                string idrow = te.ID.ToString();
                string filenamerow = Path.GetFileName(te.File.Path).ToString();
                Bitmap img = resizeBitmap((Bitmap) te.File.Thumbnail, imgsize);
                table.Rows.Add(idrow, filenamerow, img);
            }
            bindingsource1.DataSource = table;
            dataview.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        /// <summary>
        /// Helperfunctie om een bitmap te vergroten met goede kwaliteit
        /// </summary>
        /// <param name="thumbnail"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Bitmap resizeBitmap (Bitmap thumbnail, Size size)
        {
            try
            {
                Bitmap newpic = new Bitmap(size.Width, size.Height);
                using (Graphics g = Graphics.FromImage((Image)newpic))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(thumbnail, 0, 0, size.Width, size.Height);
                }
                return newpic;
            }
            catch
            {
                return thumbnail;
            }                      
        }

        /// <summary>
        /// Open een entry met een bepaalde ID
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void entryjumpen(object o, EventArgs ea)
        {
            long entrygetal;
            string entry = Interaction.InputBox("Enter an ID", "Jump to entry ...", "1", 100, 100);

            if (Int64.TryParse(entry, out entrygetal))
            {
                try
                {
                TaggyEntry entryl = new TaggyEntry(entrygetal, conn);
                    tagentryform tf = new tagentryform(entryl);
                tf.Show();
            }
                catch
                {
                    MessageBox.Show("The entry you tried to find doesn't exist", "Error");
                }
            }
            else
                MessageBox.Show("You can't search for a non-number, please search for an existing entry", "Error");
            }
            
        /// <summary>
        /// Markeer alle entries in het zoekresultaat als geselecteerd
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        public void selectallentries(object o, EventArgs ea)
        {
            dataview.SelectAll();
        }

        /// <summary>
        /// Open het TaggyEntryForm met de geselecteerde entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataview_usedatafortagform(object sender, EventArgs e)
        {
            long idfromrow;
            string idstring = dataview[0, dataview.CurrentRow.Index].Value.ToString();
            bool succes = Int64.TryParse(idstring, out idfromrow);

            if (succes)
            {
                TaggyEntry TE = new TaggyEntry(idfromrow, conn);
                tagentryform tagf = new tagentryform(TE);
                tagf.FormClosed += refresh_if_deleted;
                tagf.Show();
            }
        }

        /// <summary>
        /// Herlaadt de zoekresultaten wanneer een tagentryform sluit door verwijdering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refresh_if_deleted(object sender, EventArgs e)
        {
            tagentryform source = (tagentryform)sender;
            if (source.EntryDeleted)
                this.addtodatagrid();
        }

        /// <summary>
        /// Open het contextmenu na rechtsklikken op een rij
        /// </summary>
        /// <param name="o"></param>
        /// <param name="mea"></param>
        private void optionmenu(object o, MouseEventArgs mea)
        {
            if (mea.Button == MouseButtons.Right)
            {
                dataview.ContextMenu = cm;
                dataview.ContextMenu.Show(dataview, dataview.PointToClient(Cursor.Position));
            }
            }

        /// <summary>
        /// Voeg tags aan de geselecteerde bestanden toe
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void add_tags(object o, EventArgs ea)
        {
            DataGridViewSelectedRowCollection rows;
            rows = dataview.SelectedRows;
            List<long> ids = new List<long>();
            List<TaggyTag> taglist = TaggySearchEngine.VraagTags(conn,
            true, default_input: "tagme");
     
            foreach (DataGridViewRow row in rows)
            {
                ids.Add(long.Parse(row.Cells[0].Value.ToString()));
            }

            foreach (long id in ids)
            {
                TaggyEntry e = new TaggyEntry(id, conn);
                foreach(TaggyTag t in taglist)
                {
                    e.AddTag(t);
                }
            }
        }

        /// <summary>
        /// Toon een form waarin de gebruiker tags kan verwijderen van de geselecteerde entries
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void remove_tags(object o, EventArgs ea)
        {
            DataGridViewSelectedRowCollection rows;
            rows = dataview.SelectedRows;
            removetagsform form = new removetagsform(rows, dataview, conn);
            form.Show();
            form.FormClosed += addtodatagrid;
        }

        /// <summary>
        /// Bulk-remove meerdere entries
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void remove_entries(object o, EventArgs ea)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("Are you sure you want to delete these entries?", "warning", buttons);

            if (result == DialogResult.Yes)
            {
                DataGridViewSelectedRowCollection rows;
                rows = dataview.SelectedRows;
                List<long> ids = new List<long>();

                foreach (DataGridViewRow row in rows)
                {
                    ids.Add(long.Parse(row.Cells[0].Value.ToString()));
                }

                foreach (long id in ids)
                {
                    TaggyEntry e = new TaggyEntry(id, conn);
                    e.Delete();
                }       
            }
            addtodatagrid();
        }

        /// <summary>
        /// Toon het helpscherm
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void helpoverview(object o, EventArgs ea)
        {
            Form help = new Form();
            Button ok = new Button();
            Label text = new Label();
            text.Text = "You can search for entries by typing tag(s) in the searchbar and press enter or click the searchicon. The results of your searchcommand will be displayed below the searchbar." + Environment.NewLine +
            Environment.NewLine + "By selecting one or more entries and rightclick on them, you can add or remove tags from the entry or entries you selected and you van remove the entry or entries itself." + Environment.NewLine +
            Environment.NewLine + "By doubleclicking an entry you will go to another screen where you can easely add or remove tags to/from this entry." + Environment.NewLine +
            Environment.NewLine + "Under the tab file, you can add a file, multiple files or even a folder to this application and start adding tags to it. If you want to import files from a USB-Stick, we recommand you use the Import Single File..., Import Multiple Files... or Import Folder... function. You can also export one or multiple searchresults to a prefered folder." + Environment.NewLine +
            Environment.NewLine + "Under the tab Edit, you can select all files that are displayed under the searchbar and with delete entry you can delete the selected files you see under the searchbar. This function is only available when there are searchresults displayed." + Environment.NewLine +
            Environment.NewLine + "Under the tab Database Maintenance you will find: jump to entry... with which you can typ in the entrynumber of a file and start adding or removing tags from that file. You will also find: Manage indexed folders... where you can add folders which will be indexed everytime you start Taggy." + Environment.NewLine +
            Environment.NewLine + "For more information see our instructions.";
            text.Location = new Point(10, 10);
            text.Size = new Size(470, 300);
            ok.Location = new Point(200, 320);
            ok.Text = "ok";
            ok.DialogResult = DialogResult.OK;
            help.Size = new Size(500, 390);
            help.Text = "Help";
            help.FormBorderStyle = FormBorderStyle.FixedDialog;
            help.AcceptButton = ok;
            help.StartPosition = FormStartPosition.CenterParent;
            help.MaximizeBox = false;
            help.MinimizeBox = false;
            help.Controls.Add(text);
            help.Controls.Add(ok);
            help.ShowDialog();

            if (help.DialogResult == DialogResult.OK)
            {
                return;
            }
        }

        /// <summary>
        /// Voer een export uit van de geselecteerde bestanden
        /// </summary>
        /// <param name="o"></param>
        /// <param name="ea"></param>
        private void Click_Export(object o, EventArgs ea)
        {
            if (searchbox.Text != "Search")
            {   
                try
                {
                    FolderBrowserDialog brd = new FolderBrowserDialog();
                    if (brd.ShowDialog() == DialogResult.OK)
                    {
                        string foldername = brd.SelectedPath;
                        foreach (DataGridViewRow row in dataview.SelectedRows)
                        {
                            long id = long.Parse(row.Cells[0].Value.ToString());
                                TaggyEntry e = new TaggyEntry(id, conn);
                                var name = Path.GetFileName(e.File.Path);
                                var path = Path.Combine(foldername, name);
                                var source = e.File.Path;
                                File.Copy(source, path);
                        }
                        MessageBox.Show("File(s) have been saved in the selected folder");
                    }
                }
                catch
                {
                    MessageBox.Show("A file could not be copied.");
                }
        }
            else
            {
                MessageBox.Show("No files are selected.");
            }
        }
    }
}
